Skrypty pod Linuxa
Skrypt w bashu na zaliczenie przedmiotu Systemy Operacyjne.
Dane wejściowe:
Adam Blue:111121212211121211121212123211111212121211212311111112113312212121231212
Jerzy Nielot:111121212211121211121212123212321212122222212312411212312332212121231212

Zadanie i dane wyjściowe:
przygotować plik wynikowy: wyniki.txt, w którym zapisane zostaną nastepujące dane:

Adam Blue:111121212211121211121212123211111212121211212311111112113312212121231212:24:27:25:31:107
Jerzy Nielot:111121212211121211121212123212321212122222212312411212312332212121231212:24:31:33:34:122
W każdym wierszu liczby są sumami kolejnych 18 cyfr. 
Na końcu linii powinna być zapisana suma wszystkich cyfr.

skrypt można ururchomić następująco:
Pierwszy krok: nadanie uprawnień komendą chmod u+x pc2
Drugi krok: uruchomienie komendą z parametrem czyli z nazwą pliku który ma zostać przetworzony: ./pc2 [nazwa_pliku]